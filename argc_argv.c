int main( int argc, char **argv )
{
	/* argc is argument count passed at the terminal
	   **argv is a pointer to the pointer array of argument characters
	*/
	printf("argc = %d\n", argc);
	printf("argv = %p\n", *argv);
	printf("argv = %c\n", *(*argv+2));

}