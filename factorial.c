#include <stdio.h>
#include <string.h>

double factorial( double n );



int main( int argc, char *argv[] )
{	
	printf("%.0lf\n\n", factorial(26));
	return 0;
}

double factorial( double n ){
	if( n == 0 )
		return 1;
	else 
		return (n * factorial(n-1));
}